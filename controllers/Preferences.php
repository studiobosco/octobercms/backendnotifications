<?php namespace StudioBosco\BackendNotifications\Controllers;

use Lang;
use Flash;
use Backend;
use BackendMenu;
use Backend\Classes\Controller;
use System\Classes\SettingsManager;
use StudioBosco\BackendNotifications\Models\Preference as PreferenceModel;

class Preferences extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
    ];

    /**
     * @var array `FormController` configuration.
     */
    public $formConfig = 'config_form.yaml';

    /**
     * @var array Permissions required to view this page.
     */
    public $requiredPermissions = [
        'studiobosco.backendnotifications::manage_notifications',
    ];

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('StudioBosco.BackendNotifications', 'preferences');
        SettingsManager::setContext('StudioBosco.BackendNotifications', 'preferences');
    }

    public function index()
    {
        $this->pageTitle = Lang::get('studiobosco.backendnotifications::lang.preferences');
        $this->asExtension('FormController')->update();
    }

    public function index_onSave()
    {
        return $this->asExtension('FormController')->update_onSave();
    }

    public function index_onResetDefault()
    {
        $model = $this->formFindModelObject();
        $model->resetDefault();

        Flash::success(Lang::get('backend::lang.form.reset_success'));

        return Backend::redirect('studiobosco/backendnotifications/preferences');
    }

    public function formFindModelObject()
    {
        return PreferenceModel::instance();
    }
}
