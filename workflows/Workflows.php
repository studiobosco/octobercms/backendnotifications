<?php namespace StudioBosco\BackendNotifications\Workflows;

use StudioBosco\Workflows\Classes\NodeManager;
use StudioBosco\BackendNotifications\Workflows\Nodes\NotifyNode;

class Workflows
{
    public static function register()
    {
        NodeManager::registerNodes([
            NotifyNode::class,
        ]);

        NodeManager::registerModelClasses([
            'StudioBosco\BackendNotifications\Models\Notification' => trans('studiobosco.backendnotifications::lang.plugin.name') . ': ' . trans('studiobosco.backendnotifications::lang.notification'),
        ]);
    }
}
