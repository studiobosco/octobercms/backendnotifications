<?php namespace StudioBosco\BackendNotifications\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class V1_0_0 extends Migration
{
    public function up()
    {
        Schema::create('studiobosco_backendnotifications_notifications', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('subject')->nullable();
            $table->text('body')->nullable();
            $table->datetime('read_at')->nullable();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->string('url', 1024)->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('studiobosco_backendnotifications_notifications');
    }
}
