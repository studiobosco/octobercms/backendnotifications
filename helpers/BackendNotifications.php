<?php namespace StudioBosco\BackendNotifications\Helpers;

use Schema;
use Carbon\Carbon;
use StudioBosco\BackendNotifications\Models\Notification;
use StudioBosco\BackendNotifications\Models\Preference;
use Backend\Models\User;

/**
 * BackendNotifications Helper
 *
 * @package october\backend
 * @see \Backend\Facades\Backend
 */
class BackendNotifications
{
    /**
     * Returns notification count for current user
     * @return int Count of notifications
     */
    public static function getCount()
    {
        if (!self::isInstalled()) return 0;
        Notification::runQueue();

        return Notification::listBackend()->count();
    }

    /**
     * Returns latest unread notifications
     * @param  int $count (default = 3) max count of notifications
     * @return Collection
     */
    public static function getLatest($count = 3)
    {
        if (!self::isInstalled()) return 0;
        Notification::runQueue();

        return Notification::listBackend()
            ->limit($count)
            ->get();
    }

    /**
     * Returns latest unread notifications
     *
     * @param  Carbon $date Date since when to look for new notifications
     * @param  int $count (default = 3) max count of notifications
     * @return Collection
     */
    public static function getLatestSince(Carbon $date, $count = 3)
    {
        if (!self::isInstalled()) return 0;
        Notification::runQueue();

        return Notification::listBackend()
            ->where('created_at', '>', $date)
            ->limit($count)
            ->get();
    }

    /**
     * Notifies a user
     * @param  mixed $user    A Backend\Models\User or a user ID
     * @param  string $subject Notification Subject (will be displayed in push notifications)
     * @param  string $body    (optional) Message body
     * @param  string $url     (optional) Url to a target
     * @param  string $key     (optional) if set, notifications with the same key will be debounced
     * @return Notification  A notificaion instance or null (e.g. if it was queued)
     */
    public static function notify(
        $user,
        string $subject,
        string $body = null,
        string $url = null,
        string $key = null
    ) {
        if (!self::isInstalled()) return null;

        return $key
        ? Notification::queue([
            'user_id' => is_object($user) ? $user->id : $user,
            'subject' => $subject,
            'body' => $body,
            'url' => $url,
            'key' => $key,
        ])
        : Notification::create([
            'user_id' => is_object($user) ? $user->id : $user,
            'subject' => $subject,
            'body' => $body,
            'url' => $url,
        ]);
    }

    /**
     * Notifies all users with a global notification
     * @param  string $subject Notification Subject (will be displayed in push notifications)
     * @param  string $body    (optional) Message body
     * @param  string $url     (optional) Url to a target
     * @return Notification  A notificaion instance or null (e.g. if it was queued)
     */
    public static function globalNotifyAll(
        string $subject,
        string $body = null,
        string $url = null
    ) {
        if (!self::isInstalled()) return null;

        foreach(User::all() as $user) {
            Notification::create([
                'user_id' => $user->id,
                'subject' => $subject,
                'body' => $body,
                'url' => $url,
                'type' => 'global',
            ]);
        }
    }

    /**
     * Marks a notification as read
     * @param  mixed $notification A StudioBosco\BackendNotifications\Notification or a notification ID
     * @return  Notification|null  the notificaion instance or null
     */
    public static function read($notification)
    {
        $notification = is_object($notification)
            ? $notification : Notification::find($notification);

        if (!$notification) {
            return null;
        }

        $notification->read();
        return $notification;
    }

    public static function readAll()
    {
        $date = new Carbon();
        Notification::listBackend()
            ->update(['read_at' => $date]);

        return true;
    }

    public static function isInstalled()
    {
        return Schema::hasTable('studiobosco_backendnotifications_notifications');
    }

    public static function getPreferences()
    {
        $preferences = Preference::instance();
        return $preferences;
    }
}
